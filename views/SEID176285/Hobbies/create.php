<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>

<div style="height: 30px"> <d id="message" class="btn-danger text-center"><?php echo Message::message(); ?> </div>

<div class="container" style="background-color: whitesmoke; margin-top: 30px">
    <h1 align="center">Hobbies Form</h1>
    <form action="store.php" method="post">

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter your name Here...">
        </div>
        <div class="form-group">
            <label class="control-label">Hobbies</label>
            <br><br>
            <input type="checkbox" name="hobbies[]" value="gardening" >
            <label for="gardening">Gardening</label>

            <input type="checkbox" name="hobbies[]" value="photography">
            <label for="photography">Photography</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script>
        $(function ($) {
            $("#message").fadeOut(500);
            $("#message").fadeIn(500);

        })
    </script>

</body>
</html>