<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="background-color: whitesmoke; margin-top: 30px">
    <div class="col-md-2"> </div>
    <div class="col-md-8" style="margin-top: 50px; margin-bottom: 50px">
    <h1 align="center">Profile Picture</h1>
    <form action="store.php" method="post">

        <div class="form-group">
            <label for="name">Customer Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter your name Here...">
        </div>
        <div class="form-group">
            <label for="picture">Upload Your Profile Picture</label>
            <input type="file" name="picture">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>


    <div class="col-md-2" > </div>
</div>

</body>
</html>