<?php

namespace App\BirthDay;


use App\Model\Database;

class BirthDay extends Database
{
    public $id, $name, $date;


    public function setData ($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray['id'];

        if(array_key_exists("name",$postArray))
            $this->name = $postArray['name'];

        if(array_key_exists("date",$postArray))
            $this->date = $postArray['date'];


    } //end of setData method
    public function store(){

        //$sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES (?,?)";

        $sqlQuery = "INSERT INTO birth_day (name, date) VALUES (?,?)";

        $dataArray = [$this->name, $this->date];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);

        if($status){

            echo "Your date Of birth has been stored successfully<br>";
        }
        else
            echo "Failed! your date Of birth has not been stored<br>";


    }
}