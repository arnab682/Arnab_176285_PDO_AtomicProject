<?php

namespace App\BookTitle;


use App\Model\Database;

class Book_Title extends Database
{
    public $id, $bookTitle, $authorName;


    public function setData ($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray['id'];

        if(array_key_exists("BookTitle",$postArray))
            $this->bookTitle = $postArray['BookTitle'];

        if(array_key_exists("AuthorName",$postArray))
            $this->authorName = $postArray['AuthorName'];


    } //end of setData method
    public function store(){

        $sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES (?,?)";

        $dataArray = [$this->bookTitle, $this->authorName];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);

        if($status){

            echo "Data has been inserted successfully<br>";
        }
        else
            echo "Failed! Data has not been inserted<br>";


    }   //end of store method

} // end of class

