<?php

namespace App\Model;
use PDO, PDOException;

class Database
{
    protected $dbh;

    public function __construct()
    {
        try{

            $this->dbh = new PDO("mysql:host=localhost;dbname=atomic_project_b68", "root", "");
            $this->dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            //echo "DataBase Connection Established <br>";
        }
        catch (PDOException $error ){
            $error->getMessage();
        }
    }
}